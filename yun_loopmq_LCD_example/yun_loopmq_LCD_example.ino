
/*
  Litmus Loop
*/

/*
  Inlcude all the library files
*/
#include <loopmq.h>
#include <Bridge.h>
#include <BridgeClient.h>
#include <SPI.h>
#include <stdlib.h>
#include <stdio.h>
#include <ArduinoJson.h>
#include "configuration.h"

#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;                                  // Instantiation of the device( LCD in this demo)

const int colorR = 0;                         // Set Red color intensity 255 for all red
const int colorG = 255;                       // Set Green color intensity 255 for all green
const int colorB = 0;                         // Set Blue coloe intensity 255 for all blue

/*
  Passing the defined information to char
*/
int port = port_number;
char hostname[] = server;                     // Server name
char c[15] = clientID;                        // ClientID
char pass[16] = password;                     // password
char user[16] = userID;                       // username
char p[] = subTOPIC;                          // Subscribe on this topic to get the data
char s[] = pubTopic;                          // Publish on this topic to send data or command to device

char var[16];                                 // data published on the topic

int num = 0;                                  // intermediate variable for the counter function

/*
  Function to generate a counter to be published on a particular topic
*/
char *counter() {
  while (1) {
    if (num < 100 ) {
      num++;
      //  delay(10);
      sprintf(var, "%d", num);
      return var;
    }
    else if (num = 100)
      num = 0;
  }
}


/*
  Function to display the message that are given by the user to the device.
*/

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");


  /*
    Print the messages that are given by the user to the LCD
  */

   lcd.clear();                                // clear the LCD
   lcd.print(topic);                           // Display the topic published
   lcd.print(":");                             // Display ":"
   lcd.setCursor(1, 1);                        // Set cursor to the new line

  /*
    Display the message published serially and on the LCD
  */
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);           // Serial Display
    lcd.print((char)payload[i]);              //LCD Display
  }
  Serial.println();                         //Print on a new line
}


BridgeClient yunClient;                       // yun client
PubSubClient loopmq(yunClient);               // instance to use the loopmq functions.

void reconnect() {

  while (!loopmq.connected()) {               // Loop until we're reconnected
    Serial.print("Attempting MQTT connection...");    // Attempt to connect
    if (loopmq.connect(c,user,pass)) {
      Serial.println("connected");            // Once connected, publish an announcement...

        loopmq.subscribe(s);                 // Subscribe to a topic
        /*
          To Unscubscribe from a Topic uncomment the code below
        */

        // loopmq.unsubscribe(s);

      /*
        To disconnect unocmment the code below
      */

      //  loopmq.disconnect();
    }
    else {
      Serial.print("failed, rc=");
      Serial.print(loopmq.state());
      Serial.println(" try again in 5 seconds");

      delay(5000);                            // Wait 5 seconds before retrying
    }
  }
}

void setup()
{
 /*
   Bridge takes about two seconds to start up
   it can be helpful to use the on-board LED
   as an indicator for when it has initialized
 */  
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  Serial.begin(57600);

     lcd.begin(14, 2);                         // Set up the LCD's number of columns and rows

     lcd.setRGB(colorR, colorG, colorB);       // Set the color defined to the LCD

  loopmq.setServer(hostname, port);           // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published

  //  Ethernet.begin(mac);                      // Connect to internet based on the defined MAC


  delay(1500);                                // Allow the hardware to sort itself out

}

void loop()
{
  /*
    JSON parser
  */
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value.

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "sensor";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;
  root["number"] = var;

  JsonObject& data = root.createNestedObject("data"); // nested JSON
  data["number"] = var;                              // Add data["key"]= value
  data["Litmus"] = "Loop";


  root.printTo(Serial);                           // prints to serial terminal
  Serial.println();

  char buffer[100];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload

  /*
    Publish to the server
  */

  if (!loopmq.connected()) {
    reconnect();                              // Try to reconnect if connection dropped
  }
  if (loopmq.connect(c, user, pass)) {
    counter();

    loopmq.publish(p, buffer);                    // Publish message to the server once only
    delay(1000);
  }

  loopmq.loop();                              // check if the network is connected and also if the client is up and running
}






