# Test connection using MqttLens
MqttLens is a Google Chrome application, which connects to a MQTT broker and is able to subscribe and publish to MQTT topics. 
______

 1. Make sure you have Google Chrome or a Chrome variant installed and that you're connected to the Internet.
 2. Download MqttLens as a Google Chrome plugin. Click [here](https://chrome.google.com/webstore/detail/mqttlens/hemojaaeigabkbcookmlgmdigohjobjm) to download. Once installed, navigate to the Chrome App launcher in your browser and run MQTTLens.
 3. With MQTTLens started, we are presented with the option to connect MQTTLens to an MQTT broker. For this you will need information about the broker, for this tutorial we will use LoopDemo
 4. Input broker connection details: 
> |Connection name|  LoopDemo
>|---|---|
>|  Host name | loopdocker1.cloudapp.net |  
>| Port  | 1883  |   
>| Client ID  |  *Automatically generated* | 
>|Username|*Leave it blank as its is open source*|
>|Password|*Leave it blank as its is open source*|

 5. Click create connection.
 6.  Once you click create connection you will be taken to the screen where in you will need to provide the publish and subscribe information to test the broker or to work with the broker.
 7. Input information: 
 > |Topic|  arduino_yun/#|
> |---|---|
>|  Message | Enter any plain text or json format value  |  

In the above example you could realize that the *“arduino_yun/#”* symbol in the Subscribe input box helps you to subscribe to the device. You can look at the data in both Json format and plain text message.
8. Information on the message can be obtained by clicking the information button on the right hand side of the message.