
#ifndef configuration_h
#define configuration_h


#define port_number 1883                             // Port number
#define server "loopdocker1.cloudapp.net"            // Server name
#define clientID "yunclient"                         // ClientID
#define password "password"                          // password
#define userID "admin"                               // username 
#define subTOPIC "arduino_yun/loop1"                 // Subscribe on this topic to get the data
#define pubTopic "arduino_yun/loop2"                 // Publish on this tpoic to send data or command to device 


#endif